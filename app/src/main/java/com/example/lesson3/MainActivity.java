package com.example.lesson3;

import android.graphics.Color;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;
import java.util.logging.Handler;
import java.util.logging.LogRecord;

public class MainActivity extends AppCompatActivity {
    LinearLayout linearLayout;

    Integer pos = 0;

    LinkedList<Integer> colors = new LinkedList<>();

    {
        colors.add(Color.BLACK);
        colors.add(Color.BLUE);
        colors.add(Color.YELLOW);
        colors.add(Color.GREEN);
        colors.add(Color.CYAN);
    }

    ListIterator iterator = colors.listIterator();
    ListIterator iterator1 = (ListIterator) colors.descendingIterator();

    android.os.Handler handler = new android.os.Handler() {

        @Override
        public void dispatchMessage(Message msg) {
            if (msg.what == 1) {


                linearLayout.setBackgroundColor((Integer) iterator.next());
                handler.sendEmptyMessageDelayed(1, 1000);

            }
        }
    };

    android.os.Handler handler1 = new android.os.Handler() {
        @Override
        public void dispatchMessage(Message msg1) {
            if (msg1.what == 2) {
                linearLayout.setBackgroundColor((Integer) iterator1.next());
                handler.sendEmptyMessageDelayed(1, 1000);
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        linearLayout = (LinearLayout) findViewById(R.id.linear);


        Message msg = new Message();
        msg.what = 1;
        handler.sendMessage(msg);

        Message msg1 = new Message();
        msg1.what = 1;
        handler1.sendMessage(msg1);


    }
}
